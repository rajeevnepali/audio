from rest_framework import serializers
from .models import Audio


class AudioSerializer(serializers.ModelSerializer):
    # permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    # owner = serializers.ReadOnlyField(source='owner.username')
    class Meta:
        model = Audio
        fields = '__all__' 