 
from django.urls import path
from . import views
from django.conf.urls import include

app_name = 'api'

urlpatterns = [
    # path('audio/list/', views.AudioList.as_view()),
    path('', views.AudioList.as_view()),
]
 