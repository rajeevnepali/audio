from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from . models import Audio
from rest_framework import generics
from . serializers import AudioSerializer


class AudioList(generics.ListCreateAPIView):
    queryset = Audio.objects.all()
    serializer_class = AudioSerializer

    # def perform_create(self, serializer):
    #     serializer.save(owner=self.request.user)