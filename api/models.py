from django.db import models

# Create your models here.
class Audio(models.Model):
    title = models.CharField(max_length=100)
    file = models.FileField(upload_to='audio_file/')
    image = models.ImageField(upload_to='img', null=True, blank=True)

    def __str__(self):
        return self.title